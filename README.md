# Installer le projet
Pour se faire, il suffit de lancer la commande
```
npm install (ou npm i)
```

# Lancer le projet en local
Pour se faire, il suffit de lancer la commande
```
npm start
```

# Lancer le projet via electron
Pour se faire, il suffit de lancer la commande
```
npm run electron
```

# Packager le projet via electron
Pour se faire, il suffit de lancer la commande
```
npm run build (ou npm run electron)
```
puis la commande
```
npm run setup-win
```