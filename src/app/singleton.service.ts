import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Ticket } from './models/ticket';

@Injectable({
  providedIn: 'root',
})
export class SingletonService {
  filteredTickets: Ticket[];
  tickets: Ticket[];

  constructor(private router: Router) {}

  /**
   * Vient filtrer les tickets sur le champs "property" et la valeur "value"
   * @param value Valeur du filtre
   * @param property Champs sur lequel on filtre
   */
  filterTickets(value: string, property: string): void {
    if (!this.filteredTickets)
      this.reset();

    this.filteredTickets = this.filteredTickets.filter(ticket => ticket[property] == value);
  }

  getNextOptions(search: string, currentProperty: string, nextProperties: string[]): string[] {
    var nextOptions: string[] = [];

    nextProperties.forEach(property => {
      const matchingTickets = this.filteredTickets.filter(ticket => {
        var value: string = ticket[property];
        return value ? value.toString().toUpperCase().indexOf(search.toUpperCase()) >= 0 : false;
      });

      nextOptions = nextOptions.concat(matchingTickets.map(t => t[currentProperty]));
    });

    return nextOptions.filter((opt, i) => nextOptions.indexOf(opt) === i);
  }

  /**
   * Reset this.filteredTickets
   */
  reset(): void {
    this.filteredTickets = this.tickets;
  }

  /**
   * Force les utilisateurs à re-sélectionner leur fichier au bout de 10H
   */
  startCountdown(): void {
    setTimeout(() => {
      this.router.navigate(['']);
    }, (10 * 60 * 60 * 1000));
  }
}
