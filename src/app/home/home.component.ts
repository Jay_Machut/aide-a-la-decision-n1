import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import { Ticket } from '../models/ticket';
import { SingletonService } from '../singleton.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  // Là où se trouvent les tickets
  private firstSheet: XLSX.WorkSheet;

  // Là où se trouvent certaines procédures
  private secondSheet: XLSX.WorkSheet;

  // Là où se trouvent certains prérequis
  private thirdSheet: XLSX.WorkSheet;

  constructor(private router: Router,
    private singletonService: SingletonService,
    private snackBar: MatSnackBar) {}

  /**
   * Méthode utilisée lorsque l'utilisateur vient sélectionner un fichier excel
   * Remet à 0 les tickets enregistrés dans le service
   * Lit le fichier et vient stocker une liste de tickets
   */
  onFileSelected(): void {
    this.singletonService.tickets = [];

    const input: HTMLInputElement = document.querySelector('#file');

    if (input.files[0]) {
      const reader: FileReader = new FileReader();

      reader.onload = (e: any) => {
        const binarystr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

        try {
          this.firstSheet = wb.Sheets[wb.SheetNames[0]];
          this.secondSheet = wb.Sheets[wb.SheetNames[1]];
          this.thirdSheet = wb.Sheets[wb.SheetNames[2]];

          const data: any[] = XLSX.utils.sheet_to_json(this.firstSheet, {header: 1});

          data.forEach((row: any[], i: number) => {
            if (i >= 2) {
              let ticket: Ticket = new Ticket(row[7],
                row[4],
                row[6],
                row[8],
                row[9],
                row[10],
                this.getPrerequis(i),
                this.getProcedure(i),
                row[12].replace(/\n/g, ' '),
                row[1],
                row[3],
                row[2],
                row[5]);

              // Cas spécial, où une redirection dans le fichier a été créée (ligne 558 du fichier)
              if (ticket.ciName === '======>')
                this.treatSpecialCases(ticket, i);

              ticket.prerequis = ticket.prerequis === '-' ? '' : ticket.prerequis;

              // Si la siège vaut Partenaire / Siège, on crée 2 tickets (un Partenaire et un Siège)
              if (ticket.siege.indexOf('/') >= 0) {
                this.singletonService.tickets.push(new Ticket(ticket.appName,
                  ticket.categorie,
                  ticket.ciName,
                  ticket.groupe1,
                  ticket.groupe2,
                  ticket.groupe3,
                  ticket.prerequis,
                  ticket.procedure,
                  ticket.proprietaire,
                  'Partenaire',
                  ticket.sujet,
                  ticket.theme,
                  ticket.type));
                ticket.siege = 'Siège';
              }

              this.singletonService.tickets.push(ticket);
            }
          });
          
          this.singletonService.startCountdown();
          this.router.navigate(['select']);
        } catch(error) {
          console.log(`Error: ${error}`);
          this.snackBar.open("Le fichier sélectionné n'est pas conforme au format attendu.", "Fermer", {duration: 3000, panelClass: 'error'});
          input.value = "";
        }
      }

      reader.readAsBinaryString(input.files[0]);
    }
  }

  /**
   * Vient récupérer une cellule particulière, et s'il s'agit d'un lien vers la troisième feuille du fichier,
   * vient récupérer les données à afficher
   * @param line Numéro de la ligne (en commençant par 0)
   * @returns Le prérequis récupérer depuis la troisième feuille du fichier excel (s'il s'agit d'un lien)
   *          ou depuis la cellule directement
   */
  private getPrerequis(line: number): string {
    const cellName: string = 'L'.concat((line + 1).toString());

    if (this.firstSheet[cellName]) {
      const cell: XLSX.CellObject = this.firstSheet[cellName];

      if (cell.l) {
        if (cell.v) {
          const data: any[] = XLSX.utils.sheet_to_json(this.thirdSheet, {header: 1});
          let rel: string = (<string> (cell.v)).replace('Prérequis ', '');

          // Cas particulier, si le lien vaut FER, on renvoie en fait vers la ligne XERA (je ne sais pas pourquoi)
          if (rel === 'FER')
            rel = 'XERA';
          let result: string = '';

          for (let i = 1; i < data.length; i ++) {
            if (!data[i][0])
              data[i][0] = data[i-1][0];

            const name: string = data[i][0].toString();

            if (name.toUpperCase().indexOf(rel.toUpperCase()) >= 0) {
              result = result === '' ? data[i][1] : result + '\n' + data[i][1];
            }
          }

          return result;
        }
      } else {
        return <string> (cell.v);
      }
    } else {
      return '';
    }
  }

  /**
   * Vient récupérer une cellule particulière, et s'il s'agit d'un lien vers la deuxième feuille du fichier,
   * vient récupérer les données à afficher
   * @param line Numéro de la ligne (en commençant par 0)
   * @returns Le prérequis récupérer depuis la deuxième feuille du fichier excel (s'il s'agit d'un lien)
   *          ou depuis la cellule directement
   */
  private getProcedure(line: number): string {
    const cellName: string = 'N'.concat((line + 1).toString());

    if (this.firstSheet[cellName]) {
      const cell: XLSX.CellObject = this.firstSheet[cellName];

      if (cell.l) {
        if (cell.l.Target.startsWith('#')) {
          const data: any[] = XLSX.utils.sheet_to_json(this.secondSheet, {header: 1});
          const rel: string = cell.l.Target.substr(cell.l.Target.indexOf('_') + 1).replace('_', ' ');
          const label: string = 'Renvoyer vers '.concat(rel).concat(' (');

          for (let i = 1; i < data.length; i ++) {
            const communication: string = data[i][5];

            if (communication) {
              if (communication.toUpperCase() !== 'MAIL') {
                const name: string = data[i][0];

                if (rel.toUpperCase() === name.toUpperCase()) {
                  // si on a une adresse mail, on la renvoie
                  if (data[i][4]) {
                    return label.concat(data[i][4]).concat(')');
                  }
                  // sinon, on prend un des 2 numéros de téléphone dispo
                  else {
                    return data[i][2] ? label.concat('0' + data[i][2]).concat(')') : label.concat('0' + data[i][3]).concat(')');
                  }
                }
              }
            }
          }
        } else {
          // \\vwfrviss00030\Base_de_Co\Matrice est le chemin dans lequel se trouve leur fichier
          return '\\\\vwfrviss00030\\Base_de_Co\\Matrice\\'.concat(cell.l.Target.replace(/%20/g, ' ').replace(/\//g, '\\'));
        }
      } else {
        return <string> (cell.v);
      }
    } else {
      return '';
    }
  }

  /**
   * Vient traiter un cas spécial (ligne 558 du fichier) dans lequel il faut prendre les données d'une autre ligne
   * @param ticket Le ticket dont les valeurs sont à modifier
   * @param line La ligne du ticket en question
   */
  private treatSpecialCases(ticket: Ticket, line: number): void {
    const cellName: string = 'L'.concat((line + 1).toString());
    const cell: XLSX.CellObject = this.firstSheet[cellName];
    const target: string[] = cell.l.Target.replace('#', '').split('_');

    const data: any[] = XLSX.utils.sheet_to_json(this.firstSheet, {header: 1});

    data.forEach((row: any[], i: number) => {
      const sujet: string = <string> row[3];
      const categorie: string = row[4];

      if (sujet === target[0] && categorie.indexOf(target[1]) >= 0) {
        ticket.ciName = row[6];
        ticket.appName = row[7];
        ticket.groupe1 = row[8];
        ticket.groupe2 = row[9];
        ticket.groupe3 = row[10];
        ticket.prerequis = this.getPrerequis(i);
      }
    });
  }
}
