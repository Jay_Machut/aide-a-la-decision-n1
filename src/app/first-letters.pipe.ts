import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstLetters'
})
export class FirstLettersPipe implements PipeTransform {
  transform(value: string, index: number): string {
    if (index <= 1)
      return value;
    else
      return value.substring(0, 10);
  }
}
