import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Ticket } from '../models/ticket';
import { SingletonService } from '../singleton.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  // Tableaux contenant les champs à afficher côté html
  firstFields: string[] = ['siege', 'theme', 'sujet', 'categorie', 'type', 'ciName', 'appName',
    'groupe1', 'groupe2', 'groupe3'];
  secondFields: string[] = ['prerequis', 'proprietaire', 'procedure', 'impact'];

  // Tableaux contenant les titres à afficher côté html
  firstLabels: string[] = ['Siège/Partenaire', 'Thème', 'Sujet', 'Catégorie', 'Ticket Type',
    'CI-Name', 'Application Name', 'Assignment Group 1', 'Assignment Group 2', 'Assignment Group 3'];
  secondLabels: string [] = ['Prérequis/Liste', 'Propriétaire DSI', 'Procédure/Commentaire', 'Impact'];

  // Impact sélectionné par l'utilisateur (ou en auto)
  impact: number;

  // Labels des différents impacts
  impactLabels: string[] = ['1 - Critical\t', '2 - High\t', '3 - Middle\t', '4 - Standard\t'];

  /**
   * Vient stocker la localisation sélectionnée par l'utilisateur
   * Ce champs n'est utilisé que si ticket.groupe2 ressemble à 'ASS-SITE'
   */
  location: string;

  // Prérequis du ticket que l'on affiche, sert de snapshot si on modifie le prérequis du ticket
  prerequisSnapshot: string;

  // Vient afficher ou non le champs pour setter this.location
  showLocation: boolean = false;

  // Le ticket à afficher
  ticket: Ticket;

  // Le type du ticket (si Incident / Request, l'utilisateur doit choisir entre les 2)
  type: string;

  constructor(private router: Router,
    private singletonService: SingletonService,
    private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    // Si pas de tickets, retour au début
    if (!this.singletonService.tickets)
      this.router.navigate(['']);
    else {
      if (this.singletonService.filteredTickets.length === 1) {
        // On utilise Object.create pour avoir un clone et ne pas modifier ce qui se trouve dans le singleton
        this.ticket = Object.create(this.singletonService.filteredTickets[0]);
        this.type = this.ticket.type;
        this.prerequisSnapshot = this.ticket.prerequis;
        this.updatePrerequis();

        // Cas spécifique du groupe2 commençant par ASS-SITE
        if (this.ticket.groupe2)
          this.showLocation = this.ticket.groupe2.startsWith('ASS-SITE');

        // Cas de l'impact sélectionné en automatique, si c'est Incident l'utilisateur devra choisir
        if (this.type === 'Request')
          this.impact = 3;
      }
    }
  }

  /**
   * Permet un retour à la page de sélection de ticket (à partir de 0)
   */
  onAccueil(): void {
    this.router.navigate(['select']);
  }

  /**
   * Permet un retour à la page de sélection de ticket (à partir du dernier choix)
   */
  onBack(): void {
    this.router.navigate(['select'], {queryParams: {fromSummary: true}});
  }

  /**
   * Vient setter this.impact avec le choix de l'utilisateur
   * @param impact Impact sélectionné par l'utilisateur (0, 1, 2 ou 2)
   */
  onChangeImpact(impact: number): void {
    this.impact = impact;
  }

  /**
   * Vient setter this.location avec le choix de l'utilisateur
   * @param location Location sélectionnée par l'utilisateur (VC, FDT ou ROI)
   */
  onChangeLocation(location: string): void {
    this.location = location;
  }

  /**
   * Vient réinitialiser this.impact, puis update le prérequis du ticket en fonction du type choisi
   * @param type Type sélectionné par l'utilisateur (Incident ou Request)
   */
  onChangeType(type: string): void {
    this.impact = undefined;
    this.type = type;
    this.updatePrerequis();
    
    if (this.type === 'Request')
      this.impact = 3;
  }

  /**
   * Méthode utilisée sur les divers boutons de copie pour informer l'utilisateur
   */
  onCopy(): void {
    this.snackbar.open("Information copiée !", 'Fermer', {duration: 1500});
  }

  /**
   * Update le prérequis du ticket en fonction du type du ticket, et de son prérequis
   */
  updatePrerequis(): void {
    this.ticket.prerequis = this.prerequisSnapshot;

    if (this.type === 'Incident') {
      if (this.ticket.prerequis !== '')
        this.ticket.prerequis += '\n**********************\nDiagnostic :';
      else
        this.ticket.prerequis += 'Diagnostic :\n**********************\nMessage d\'erreur :\n**********************\nChemin :';
    }

    if (this.type === 'Request')
      if (this.ticket.prerequis === '')
        this.ticket.prerequis = 'Détail de la demande :';
  }
}
