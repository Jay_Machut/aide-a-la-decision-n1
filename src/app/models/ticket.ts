export class Ticket {
  appName: string;
  categorie: string;
  ciName: string;
  groupe1: string;
  groupe2: string;
  groupe3: string;
  prerequis: string;
  procedure: string;
  proprietaire: string;
  siege: string;
  sujet: string;
  theme: string;
  type: string;

  constructor(appName: string,
    categorie: string,
    ciName: string,
    groupe1: string,
    groupe2: string,
    groupe3: string,
    prerequis: string,
    procedure: string,
    proprietaire: string,
    siege: string,
    sujet: string,
    theme: string,
    type: string) {
      this.appName = appName;
      this.categorie = categorie;
      this.ciName = ciName;
      this.groupe1 = groupe1;
      this.groupe2 = groupe2;
      this.groupe3 = groupe3;
      this.prerequis = prerequis;
      this.procedure = procedure;
      this.proprietaire = proprietaire;
      this.siege = siege;
      this.sujet = sujet;
      this.theme = theme;
      this.type = type;
    }
}