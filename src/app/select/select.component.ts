import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from '../models/ticket';
import { SingletonService } from '../singleton.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  
  /**
   * Tableau dans lequel on vient stocker les différentes options disponibles en fonction des choix de l'utilisateur
   * On vient le filtrer selon la recherche de l'utilisateur
   */
  filteredOptions: string[] = [];

  // 3 Tableaux utilisés pour split celui au-dessus en plus petit (pour un affichage en colonne)
  filteredOptions1: string[] = [];
  filteredOptions2: string[] = [];
  filteredOptions3: string[] = [];

  // True si on a fait un retour arrière depuis la page summary (on reprend pas de 0)
  fromSummary: boolean = false;

  // 0 quand on a pas fait de choix, puis +1 à chaque choix, ou -1 pour chaque retour arrière
  index: number = 0;

  /**
   * Tableau dans lequel on vient stocker les différentes options disponibles en fonction des choix de l'utilisateur
   * On ne filtre pas celui-ci, il sert de base pour filteredOptions
   */
  options: string[] = [];

  // Tableau dans lequel on vient mettre les choix de l'utilisateur
  selectedOptions: string[] = [];

  // Différents titres à afficher (selon this.index)
  titles: string[] = ['Siège', 'Thème', 'Sujet', 'Catégorie'];

  // Champs des tickets que l'on doit filtrer, selon le nombre de choix fait (this.index)
  private order: string[] = ['siege', 'theme', 'sujet', 'categorie'];

  // Liste de tickets
  private tickets: Ticket[];

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private singletonService: SingletonService) {
      this.activatedRoute.queryParams.subscribe(params => {
        if (params['fromSummary'])
          this.fromSummary = params['fromSummary'];
      });
    }

  ngOnInit(): void {
    this.init();
  }

  /**
   * Méthode utilisée pour revenir à l'accueil (page de base avec this.index = 0)
   */
  onAccueil() : void {
    this.index = 0;
    this.selectedOptions = [];
    this.resetSearch();
    this.init();
  }

  /**
   * Méthode utilisée pour revenir une étape en arrière
   */
  onBack(): void {
    this.index--;
    this.selectedOptions.splice(this.selectedOptions.length - 1);

    this.goBack();
  }

  /**
   * Méthode utilisée pour revenir une ou plusieurs étapes en arrière depuis le fil d'Ariane
   * @param selected Valeur du fil d'Ariane sélectionnée
   */
  onBreadcrumb(selected: string): void {
    const i = this.selectedOptions.indexOf(selected);
    this.index = i;
    this.selectedOptions.splice(i);

    this.goBack();
  }

  /**
   * Méthode utilisée pour filtrer les options
   * @param value Valeur du champs de recherche
   */
  onChange(value: string): void {
    if (value) {
      this.filteredOptions = this.options.filter(opt => opt.toString().toUpperCase().indexOf(value.toUpperCase()) >= 0);

      if (this.filteredOptions.length === 0)
        this.filteredOptions = this.singletonService.getNextOptions(value, this.order[this.index], this.order.slice(this.index + 1));

      this.filteredOptions.sort();
      this.splitOptions();
    } else {
      this.resetFilter();
    }
  }

  /**
   * Méthode utilisée quand l'utilisateur fait un choix
   * On vient filtrer les tickets du service, puis si c'était le dernier choix on va à la page summary, sinon on récupère les options possibles
   * @param value Option sélectionnée par l'utilisateur
   */
  onSelect(value: string): void {
    this.selectedOptions.push(value);
    this.singletonService.filterTickets(value, this.order[this.index]);

    if (this.index === 3) {
      this.router.navigate(['summary']);
    } else {
      this.tickets = this.singletonService.filteredTickets;
      this.resetSearch();

      this.index++;
      this.getOptions();
    }
  }

  /**
   * Méthode utilisée pour récupérer les options selon l'étape (this.index) où on se trouve
   */
  private getOptions(): void {
    const options: string[] = this.tickets.map(ticket => ticket[this.order[this.index]]);
    this.options = options.filter((opt, i) => options.indexOf(opt) === i && opt)
      .map(opt => opt.toString().trim());

    this.resetFilter();
  }

  /**
   * Méthode utilisée lors d'un retour en arrière
   * On vient simplement refiltrer les tickets selon les choix restants
   */
  private goBack(): void {
    this.singletonService.reset();
    this.resetSearch();

    if (this.index > 0) {
      for (let i = 0; i < this.index; i++) {
        this.singletonService.filterTickets(this.selectedOptions[i], this.order[i]);
      }

      this.tickets = this.singletonService.filteredTickets;
      this.getOptions();
    } else {
      this.init();
    }
  }

  /**
   * Méthode appelée lorsque l'on arrive sur la page où que l'on retourne à this.index === 0
   * Si on a plus de ticket dans le service (cas du reload de la page), on retourne à la sélection de l'excel
   * Sinon si on ne vient pas de la page summary on remet tout à 0
   * Et si on vient de la page summary, on récupère les choix de l'utilisateur depuis le seul ticket restant dans le service
   */
  private init(): void {
    if (!this.singletonService.tickets)
      this.router.navigate(['']);
    else {
      if (!this.fromSummary) {
        this.singletonService.reset();
        this.tickets = this.singletonService.tickets;

        this.getOptions();
      } else {
        this.index = 3;
        this.fromSummary = false;

        this.tickets = this.singletonService.filteredTickets;
        const ticket = this.tickets[0];
        this.selectedOptions[0] = ticket.siege;
        this.selectedOptions[1] = ticket.theme;
        this.selectedOptions[2] = ticket.sujet;
        this.goBack();
      }
    }
  }

  /**
   * Remet le tableau d'options filtrées à 0
   */
  private resetFilter(): void {
    this.filteredOptions = this.options;
    this.filteredOptions.sort();

    this.splitOptions();
  }

  /**
   * Remet à 0 le champs de recherche (lors d'un retour arrière ou de la sélection d'une option)
   */
  private resetSearch(): void {
    const input: HTMLInputElement = <HTMLInputElement> document.getElementById('search');
    input.value = '';
  }

  /**
   * Méthode utilisée lorsque l'on touche à this.filteredOptions
   * Selon la taille du tableau, vient le split en 2 voir 3 plus petits pour afficher le tout en colonne
   */
  private splitOptions(): void {
    const length: number = this.filteredOptions.length;
    this.filteredOptions1 = [];
    this.filteredOptions2 = [];
    this.filteredOptions3 = [];
    
    if (length > 6 && length < 12) {
      this.filteredOptions1 = this.filteredOptions.slice(0, 6);
      this.filteredOptions2 = this.filteredOptions.slice(6);
    } else {
      if (length >= 12) {
        let firstThird: number = Math.floor(Math.floor(length / 3));
        if (length % 3 === 1 || length % 3 === 2)
          firstThird ++;
        
        let secondThird: number = Math.floor(Math.floor(length / 3)) + firstThird;
        if (length % 3 === 2)
          secondThird ++;

        let lastThird: number = Math.floor(Math.floor(length / 3)) + secondThird;
        
        this.filteredOptions1 = this.filteredOptions.slice(0, firstThird);
        this.filteredOptions2 = this.filteredOptions.slice(firstThird, secondThird);
        this.filteredOptions3 = this.filteredOptions.slice(secondThird, lastThird);
      }
    }
  }
}
